<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryStateCityTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('grizzdev_countries', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 30);
			$table->boolean('enabled');
		});

		Schema::create('grizzdev_states', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 30);
			$table->boolean('enabled');
			$table->integer('grizzdev_country_id');
		});

		Schema::create('grizzdev_cities', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 30);
			$table->integer('grizzdev_state_id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('grizzdev_countries');
		Schema::drop('grizzdev_states');
		Schema::drop('grizzdev_cities');
	}

}
