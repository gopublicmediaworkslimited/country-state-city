<?php
namespace GrizzDev\CSC\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	protected $table = 'grizzdev_countries';

	public function states() {
		return $this->hasMany('GrizzDev\CSC\Model\State', 'grizzdev_country_id');
	}

}
