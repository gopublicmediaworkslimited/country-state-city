<?php
namespace GrizzDev\CSC\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

	protected $table = 'grizzdev_cities';

	public function state() {
		return $this->belongsTo('GrizzDev\CSC\Model\State', 'grizzdev_state_id');
	}

}
