<?php
namespace GrizzDev\CSC\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

	protected $table = 'grizzdev_states';

	public function country() {
		return $this->hasOne('GrizzDev\CSC\Model\Country', 'grizzdev_country_id');
	}

	public function cities() {
		return $this->hasMany('GrizzDev\CSC\Model\City', 'grizzdev_state_id');
	}

}
