<?php

namespace GrizzDev\CSC;

use Illuminate\Support\ServiceProvider;

class CSCServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot() {
		$this->publishes([
			__DIR__.'/../../../database/migration.php' => database_path('migrations/'.date('Y_m_d').'_create_country_state_city_tables.php')
		], 'migration');

		$this->publishes([
			__DIR__.'/../../../database/seeder.php' => database_path('seeds/CountryStateCitySeeder.php')
		], 'seeder');

		$this->app->singleton('grizzdev.csc.country', function() {
			return new Model\Country;
		});

		$this->app->singleton('grizzdev.csc.state', function() {
			return new Model\State;
		});

		$this->app->singleton('grizzdev.csc.city', function() {
			return new Model\City;
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
	}

	public function provides() {
		return [
			'grizzdev.csc.country',
			'grizzdev.csc.state',
			'grizzdev.csc.city',
		];
	}

}
